# URL Shortener
*Note: In the requirements.txt you'll see listed some unused libraries that I expected to implement but due to lack of time didn't get to apply yet.*
*Note 2: Due to my time limitations, the docstrings for the views and functions were generated with ChatGPT.*
*Note 3: I pushed the .env file with Django's secret key so that you'd be able to run it, I wouldn't do that otherwise.*

## Index

### Table of Contents

- [How to run the project?](#how-to-run-the-project)
- [Running manage.py commands inside the container](#running-managepy-commands-inside-the-container)
- [How to stop the container?](#how-to-stop-the-container)
- [Technologies used](#technologies-used)
- [Project composition](#project-composition)
- [Why Docker?](#why-docker)
- [What to expect when building the image](#what-to-expect-when-building-the-image)
- [About the project](#about-the-project)
- [Frontend](#frontend)


### Technologies used

#### Backend:

- Python 3.11
- Django 4.2.7
- Django REST Framework 3.14.0
- Docker & Docker Compose
- Celery
- Redis
- Flower
- JWT Token Authentication
- Swagger

#### Frontend:

- Vite
- Javascript
- React.js
- Redux
- Thunk



### How to run the project?
- First you should clone both repositories, this one for the backend, and the one for the frontend. [Frontend Repository](https://gitlab.com/urlshortener1/frontend.git)

_For the frontend:_
- Once the repository was cloned, you should `cd` into `us_frontend` and run the command `npm install` so all the dependencies will get installed.
- After that, run `npm run dev` to start the react (Vite) development server.
- You'll be able to access to the frontend development server from: [http://localhost:5173/](http://localhost:5173/)

_For the backend:_
- If you don't have Docker, download Docker Desktop from here: [Docker Desktop](https://www.docker.com/products/docker-desktop/)
- Install it and open it (just leave it open, you only need it to initialize Docker)
- `docker-compose up` (to build the docker image and start the server)
- Once it's up and running, you'll be able to access to the backend development server from: [http://localhost:8000/](http://localhost:8000/)

If you need to run any commands from the terminal, then you must do it from the container's terminal:

- Split you terminal to the right, so in the left you'll have the container built and running, and in the right, you'll have a free terminal.
- In that free terminal on the left, run `docker exec -it us_backend bash`
- You'll see something like this: `user@b2d796ae35be:/us_backend$ ` meaning you are now inside the container (linux commands here)
- URLs: "http://127.0.0.1:8000/admin/", "http://127.0.0.1:8000/api/shortener/", 
            "http://127.0.0.1:8000/schema/swagger-ui/"

### Running manage.py commands inside the container (e.g.: python manage.py createsuperuser)
- Once you've run `docker exec -it us_backend bash` with the container up and running, you'll have to enter the dir where the manage.py is located,
in this case, "us_backend". 
- Run `cd us_backend` and then you can run all the manage.py commands you need. 


### How to stop the container?

- `Ctrl + C` in the terminal running the container.
- After that, run the command `docker-compose down`.
- If for any reason, you need to delete the container, image and container's cache, run these following commands:
```
docker image prune -a
docker volume prune
docker system prune
```
- Now the container will be completely erased.
- Most of the times you might need to do that if you modified something related to the Docker image (like something in the Dockerfile, docker-compose.yml, or the requirements.txt file).


### Project composition

The project consists of 3 main modules:
- us_backend: contains settings, global urls, and Celery configuration.
- base: contains user, authentication and BaseModel.
- shortener: contains the main logic of the app. The models to handle the Short URLs, the celery task, and the management command to populate the ShortcodePool table (further explained).


### Why Docker?
Implementing Docker allows everyone handling the project to have the exact same setup, making the developing process simpler, as also the deployment part too.
It also enhances security, isolation, scalability, portability and consistancy across environments.
You won't need to manually install all the dependencies, and by just running a single command (`docker-compose up`), you can have the project up and running, as easy as that.


### What to expect when building the image
When you build the image by running `docker-compose up`, you'll see that different services get created:
- db: This is the container for the database
- us_backend: This is the container for the whole backend project
- redis
- celery_worker: The container for the celery worker, in charge of running the task to populate the ShortcodePool table
- celery_beat: The container for the celery beat image which will take care of running a task once per hour to check if the ShortcodePool table needs to be populated
- If it's the first time running it, then the database will probably be empty, so a manager command will automatically run and populate the ShortcodePool table with 10 shortcodes (you could also run the management command manually if you do not want to wait for celery to run and populate it for you).



### About the project
This project is basically a url redirector, where the users will enter a URL (from the frontend) and get a shortened url in return.

To achieve that, users will have to log in.

For the login/registration part, I implemented JWT Token authentication as a secure way to prevent from CSRF, MitM attacks, since requires the client to include a token in each request (marked with the IsAuthenticated permission_classes) (Django ORM already prevents against XSS due to its model field validation).

- How did I handle shortId collisions?
Well, at first I thought of hashing the original url, adding a nonce to make it more secure, then truncating it, and then checking if that short url already existed. I wasn't convinced about this approach. I didn't like the fact that I was doing the check at the exact time where the short code was needed and the problems in scalability and user experience that might carry

- Final approach:
I created a ShortcodePool, and decided to add Celery and make it run once every hour, to populate that ShortcodePool with an X amount of codes (the amount can be changed) and overwrote the Django's save() method so when a user sends a post request to create a ShortURL, it would grab an already created, unique code from the ShortcodePool. 

The function (in shortener/utils.py) in charge of generating each code, creates a random uuid to use as a nonce, gets a timestamp, creates a salt from random characters, hashes all that, and truncates it to a desired length.

Once the short url is generated, there's a redirect view, which redirects the user to the original url from the short url generated by this project.


- If you access [http://127.0.0.1:8000/schema/swagger-ui/](http://127.0.0.1:8000/schema/swagger-ui/) you can see all the API endpoints in swagger.
Since they require to be authenticated, and thus a token, you should start by testing the login endpoint and copying the token from there, and then authenticating in each endpoint you wish to test, by clicking in the small padlock at the right of each endpoint, and pasting the token there. 
After that you'll be able to test the different endpoints from swagger.


### Frontend [Frontend Repository](https://gitlab.com/urlshortener1/frontend.git)
*Note: As simple as it might be, the design is 100% mine, though due to time constraints, I reused some CSS and some components from other project I did.*

The frontend is built with Vite, React.js, Redux and Thunk for handling the store and global states.
Redux is being used to globally handle the userInfo and prevent me from passing props or doing something more complicated.

Redux handles everything related to user login, registration, and theme selection (light-dark mode).
For Redux there's a store.js, which applies thunk to handle more than one global state, and there's reducers/index.js which has the different reducers for theme and userLogin.
There's also the types and actions in redux which get triggered when said action gets dispatched inside a component.

Each component has its own css file inside /styles and there's an index.css which imports all the css into it.

Regarding Pages and Components architecture, since the project was small, I opted for having a folder for components, another for pages, which contains the different components, and the layouts dir for the main layout.

The pages are imported into App.jsx and routed with react-router-dom.

The user needs to be logged in to access the home page.

- API calls are made with Axios, which has its own script (axios-api.js) for changing the url to which do the requests (127.0.0.1:8000/)

- Things I didn't have time to implement: form validation (username, password, and url input fields), dive deeper into more security meassures from the frontend side and more features for the users, like displaying all his shorturls, updating them or his profile, deleting them, etc.

