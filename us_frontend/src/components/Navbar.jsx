import { NavLink } from 'react-router-dom'
import logo from '../assets/images/plogo.png'
import { useDispatch, useSelector } from 'react-redux'
import { changeTheme } from '../redux/actions/themeActions'
import { logout } from '../redux/actions/userActions'

export const Navbar = () => {

  const { theme } = useSelector((state) => state.theme)
    const { userInfo } = useSelector((state) => state.userLogin)
    
    const dispatch = useDispatch()

    const handleChangeTheme = () => {
        if (theme == 'light') {
            dispatch(changeTheme('dark'))
        } else {
            dispatch(changeTheme('light'))
        }
    }

    const handleLogout = (e) => {
        e.preventDefault()
        dispatch(logout())
    }

  return (
      <nav className="navbar">
            <NavLink className="logo-text-container" to={'/'}>
                <div className='logo-container'>
                  <img className='logo' src={logo} alt="URL Shortener Logo" />
                </div>
                URL Shortener
            </NavLink>
        <ul className='nav-items-container'>
            <div className="togglerIconContainer" onClick={handleChangeTheme}>
                {
                    theme == 'dark' ?
                    <i className="fa-solid fa-sun togglerIcon"></i> :
                    <i className="fa-solid fa-moon togglerIcon"></i>
                }
            </div>
            {userInfo &&

            <div className='logout-btn' onClick={handleLogout}>
                <i className="fa-solid fa-power-off togglerIcon"></i>
            </div>
            }
        </ul>
      </nav>
  )
}
