import { 
    USER_LOGIN_REQUEST, 
    USER_LOGIN_SUCCESS, 
    USER_LOGIN_FAIL, 

    USER_LOGOUT_REQUEST,
    USER_LOGOUT_SUCCESS,
    USER_LOGOUT_FAIL,
    
    USER_REGISTER_REQUEST, 
    USER_REGISTER_SUCCESS, 
    USER_REGISTER_FAIL, 

    USER_DETAILS_REQUEST, 
    USER_DETAILS_SUCCESS, 
    USER_DETAILS_FAIL, 
    USER_DETAILS_RESET,

    USER_UPDATE_PROFILE_REQUEST, 
    USER_UPDATE_PROFILE_SUCCESS, 
    USER_UPDATE_PROFILE_FAIL,
    
    USER_LIST_REQUEST, 
    USER_LIST_SUCCESS, 
    USER_LIST_FAIL,
    USER_LIST_RESET,
            
    USER_DELETE_REQUEST, 
    USER_DELETE_SUCCESS, 
    USER_DELETE_FAIL,
            
    USER_UPDATE_REQUEST, 
    USER_UPDATE_SUCCESS, 
    USER_UPDATE_FAIL,
} from '../types/userTypes'

import { getAPI } from '../../axios-api'


export const login = (username, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_LOGIN_REQUEST
        })

        const config = {
            headers: {
                'Content-type' : 'application/json'
            }
        }

        const { data } = await getAPI.post(
            '/api/base/users/login/',
            {'username': username, 'password': password},
            config   
        )

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        })

        localStorage.setItem('userInfo', JSON.stringify(data))


    } catch(error) {
        dispatch({
            type: USER_LOGIN_FAIL,
            payload: error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message
        })

        
    }
}



export const logout = () => async (dispatch, getState) => {
    dispatch({type: USER_LOGOUT_REQUEST});
    
    const { userLogin: {userInfo} } = getState();
    
    if(userInfo && userInfo.token) {
        const config = {
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        };

        const body = JSON.stringify({ refresh: userInfo.refresh });
        
        try {

            await getAPI.post('/api/base/users/logout/', body, config);

            localStorage.removeItem('userInfo');
            // Assuming logout is always successful from the client's perspective,
            // as we're removing the user info regardless of the server's response.
            dispatch({type: USER_LOGOUT_SUCCESS});
            dispatch({type: USER_DETAILS_RESET});
        } catch (error) {
            console.error("Logout failed", error);
            dispatch({type: USER_LOGOUT_FAIL});
        }
    } else {
        console.log("No user info or token found for logout.");
        // Dispatch success and reset actions to clean up state as no user is logged in.
        localStorage.removeItem('userInfo');
        // If there's no user info or token, still dispatch logout success to ensure state is clean.
        dispatch({type: USER_LOGOUT_SUCCESS});
        dispatch({type: USER_DETAILS_RESET});
    }
};


export const register = (username, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_REGISTER_REQUEST
        })

        const config = {
            headers: {
                'Content-type' : 'application/json'
            }
        }

        const { data } = await getAPI.post(
            '/api/base/users/register/',
            {'username': username, 'password': password},
            config   
        )

        dispatch({
            type: USER_REGISTER_SUCCESS,
            payload: data
        })
        

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        })

        localStorage.setItem('userInfo', JSON.stringify(data))


    } catch(error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message
        })
    }
}
