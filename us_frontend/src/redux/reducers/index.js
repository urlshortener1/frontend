import { combineReducers } from "redux";
import { themeChangeReducer } from "./themeReducers";
import { userLoginReducer } from "./userReducers";



export default combineReducers({
    theme: themeChangeReducer,
    userLogin: userLoginReducer,
})