import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Navigate } from 'react-router-dom'
import { getAPI } from '../axios-api'

export const Home = () => {
  const userLogin = useSelector(state => state.userLogin)
  const { error, loading, userInfo } = userLogin

  const [url, setUrl] = useState('')
  const [finalUrl, setFinalUrl] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  const handleChange = (newVal) => {
    setUrl(newVal)
  }

  const handleSetFinalUrl = async (urlValue) => {
    if (!url) {

      setErrorMessage("You must add a URL.")

    } else {

      const config = {
        headers: {
            'Content-type' : 'application/json',
            Authorization: `Bearer ${userInfo.token}`
        }
      }
  
      const body = JSON.stringify({
        user:userInfo.id,
        url: urlValue,
      });
  
      try {
        const { data } = await getAPI.post(
          '/api/shortener/shorturls/',
          body,
          config   
        )
        setErrorMessage('')
        setFinalUrl(data.shortened_url)
        setUrl('')
        } catch (error) {
          setErrorMessage("Failed to shorten URL.")
          console.error("Failed to shorten URL:", error);
          // Handle error appropriately
      }
    }
  }
  

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log("FINAL URL BEFORE GETTING THE SHORTENED VERSION", url)
    handleSetFinalUrl(url)
  }

    

  useEffect(() => {
    
  }, [userInfo, url])

  if(!userInfo) {
    return(
      <Navigate to={'/login'}  />
    )
  } else {
    
    return (
      <div className='home-body-container'>
        <div className='shortenerbox'>
          <form className='url-form' action="POST">
            <div className='shortener-title'>
              <h2>URL Shortener</h2>
            </div>
            <div className="input-wrapper">
                <input type="text" placeholder='Your URL' value={url} onChange={(e) => handleChange(e.target.value)} />
            </div>
            <div className="url-btn-container">
                <button
                    type='submit'
                    className='form-btn url-form-button'
                    onClick={handleSubmit}
                >
                    Get Shortened URL
                </button>
            </div>
          </form>
          {finalUrl && !errorMessage &&
          <div className='final-url-container'>
            <div className='final-url-title'>
              <h2>Short URL successfully created!</h2>
            </div>
            <a href={finalUrl} target="_blank">{finalUrl}</a>
          </div>
          }

          {errorMessage &&
          <div className='final-url-container-error'>
            <div className='final-url-title-error'>
              <h2>{errorMessage}</h2>
            </div>
          </div>
          }
        </div>
      </div>
    )
  }
}
