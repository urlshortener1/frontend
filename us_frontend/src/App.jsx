import { useState } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { useSelector } from 'react-redux'

import Layout from './pages/layouts/Layout';
import { Home } from './pages/Home';
import { Login } from './pages/Login';
import { Register } from './pages/Register';


function App() {

  const router = createBrowserRouter([
    {
      path: '/',
      element: <Layout />,
      children: [
        {
          index: true,
          element: <Home />
        },
        {
          path: '/login',
          element: <Login />
        },
        {
          path: '/sign_up',
          element: <Register />
        },

      ]
    }
  ])

  // TESTEANDO REDUX:
  const { theme } = useSelector((state) => state.theme)

  const element = document.getElementsByTagName('body')[0]

  if (theme == 'light') {
    element.classList.remove('body-dark')  
    element.classList.add(`body-${theme}`)
  } else if (theme == 'dark') {
    element.classList.remove('body-light')
    element.classList.add(`body-${theme}`)
  }
  // FIN TESTEO REDUX

  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  )
}

export default App
